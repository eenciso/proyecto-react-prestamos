export function calcularTotal (cantidad, plazo) {
	let totalInteres;
	// Calcular el Interés
	if (cantidad <= 1000) {
		totalInteres = cantidad * .25;
	} else if (cantidad > 1000 && cantidad <= 5000) {
		totalInteres = cantidad * .20;
	} else if (cantidad > 5000 && cantidad <= 10000) {
		totalInteres = cantidad * .15;
	} else {
		totalInteres = cantidad * .10;
	}

	// Calcular el Plazo
	let totalPlazo = 0;
	switch (plazo) {
		case 3:
			totalPlazo = cantidad * .05;
			break;
		case 6:
			totalPlazo = cantidad * .10;
			break;
		case 12:
			totalPlazo = cantidad * .15;
			break;
		case 24:
			totalPlazo = cantidad * .20;
			break;
		default:
			// statements_def
			break;
	}
	return cantidad + totalInteres + totalPlazo;
}