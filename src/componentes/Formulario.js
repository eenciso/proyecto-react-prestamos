import React, {Fragment, useState} from 'react';
import { calcularTotal } from '../helpers.js';
// Aplicando destructuring a las funciones que vienen del padre
const Formulario = (props) => {

	const {cantidad, guardarCantidad, plazo, guardarPlazo, guardarTotal, guardarCargando} = props;

	// Definir state
	const [error, guardarError] = useState(false);

	// Función para Calcular el Prestamo
	const calcularPrestamo = e => {
		e.preventDefault();
		// Validar los datos
		if ( cantidad === 0 || plazo === '') {
			guardarError(true);
			return;
		} else {
			guardarError(false);
			guardarCargando(true);
		}

		// Muestra el Spinner durante 3s
		setTimeout( () => {
			// Realiza la cotización con la función creada en el archvio helper.js
			const total = calcularTotal(cantidad, plazo);
			// Guarda el total
			guardarTotal(total);

			// Deshabilitar el spinner
			guardarCargando(false);
		},3000);

	}

	return (
		<Fragment>
		<form onSubmit={ calcularPrestamo }>
			<div className="row">
				<div>
					<label>Cantidad Prestamo</label>
					<input className="u-full-width" type="number" placeholder="Ejemplo: 3000" onChange={ e => guardarCantidad ( parseInt(e.target.value) ) }/>
				</div>
				<div>
					<label>Plazo para Pagar</label>
					<select className="u-full-width" onChange={ e => guardarPlazo( parseInt( e.target.value ) ) }>
						<option value="">Seleccionar</option>
						<option value="3">3 meses</option>
						<option value="6">6 meses</option>
						<option value="12">12 meses</option>
						<option value="24">24 meses</option>
					</select>
				</div>
				<div>
					<input type="submit" value="Calcular" className="button-primary u-full-width" />
				</div>
			</div>
		</form>
		{ (error) ? <p className="error">Todos los campos son obligatorios.</p> : null }
		</Fragment>
	);

}

export default Formulario;