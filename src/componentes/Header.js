import React from 'react'

// El retrun va implicito
const Header = ({titulo}) => (
	<h1>{ titulo }</h1>
);

export default Header;