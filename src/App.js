import React, {Fragment, useState} from 'react';
import Header from './componentes/Header';
import Formulario from './componentes/Formulario';
import Mensaje from './componentes/Mensaje';
import Resultado from './componentes/Resultado';
import Spinner from './componentes/Spinner';

function App() {
	// Define los state
	const [cantidad, guardarCantidad] = useState(0);
	const [plazo, guardarPlazo] = useState('');
	// State para el Total
	const [total, guardarTotal] = useState(0);
	// State para el Spinner
	const [cargando, guardarCargando] = useState(false);

	// Carga Condicional de los Componentes
	let componente;
	if (cargando) {
		componente = <Spinner />
	} else if (total === 0) {
		componente = <Mensaje />
	} else {
		// Se pasa mediante props la información a mostrar
		componente = <Resultado cantidad={cantidad} plazo={plazo} total={total} />
	}

	// Se pasan la funciones via props
	return (
		<Fragment>
			<Header titulo = "Cotizador de Prestamos" />
			<div className="container">
				<Formulario
				cantidad={cantidad} guardarCantidad={guardarCantidad}
				plazo={plazo} guardarPlazo={guardarPlazo}
				total={total} guardarTotal={guardarTotal}
				guardarCargando={guardarCargando}
				/>
				<div className="mensajes">
					{componente}
				</div>
			</div>
		</Fragment>
	);
}

export default App;